const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI"; // Replace with a secure method to handle the secret key

// Token Creation
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, {});
};

// Token Verification Middleware
module.exports.verify = (req, res, next) => {
  const token = req.headers.authorization;

  if (typeof token !== "undefined") {
    jwt.verify(token.slice(7), secret, (err, data) => {
      if (err) {
        return res.sendStatus(401); // Unauthorized
      } else {
        next();
      }
    });
  } else {
    return res.sendStatus(401); // Unauthorized
  }
};

// Token Decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    return jwt.decode(token.slice(7), { complete: true }).payload;
  } else {
    return null;
  }
};

// Middleware to check if the user is an admin
module.exports.isAdmin = (req, res, next) => {
  const token = req.headers.authorization;

  if (typeof token !== "undefined") {
    const decodedToken = jwt.decode(token.slice(7));
    if (decodedToken && decodedToken.isAdmin) {
      next();
    } else {
      return res.sendStatus(403); // Forbidden
    }
  } else {
    return res.sendStatus(401); // Unauthorized
  }
};
