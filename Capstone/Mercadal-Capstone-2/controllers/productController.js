const Product = require("../models/Product")

// Controller for creating a product (admin only);

module.exports.addProduct = (data) => {

// User is an admin
if (data.isAdmin) {

let newProduct = new Product({
	name : data.product.name,
	description : data.product.description,
	price : data.product.price
	});
return newProduct.save().then((product, error) => {
	if (error) {
		return false;
		} else {
			return true;
		};
	});

// User is not an admin
} else {
	return false;
	};

};


// Controllers for retrieving all the products(admin only)
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Controllers for retrieving active products(admin not required)
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Controllers for retrieving single product(admin not required)
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Controllers for updating a product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// Controllers for archiving a product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// Controllers for reactivating a product
module.exports.reactivateProduct = (reqParams) => {
	let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// Controller to delete a product
module.exports.deleteProduct = (productId) => {
  return Product.findByIdAndDelete(productId)
    .then((product) => {
      if (product) {
        return true;
      } else {
        return false;
      }
    })
    .catch((error) => {
      console.error('Error deleting product:', error);
      throw error;
    });
};