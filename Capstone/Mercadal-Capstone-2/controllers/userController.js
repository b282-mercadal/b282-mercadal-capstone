const User = require("../models/User")

const bcrypt = require("bcrypt");

const auth = require("../auth");

const Product = require("../models/Product");

const mongoose = require("mongoose");

//Check if email already exists

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};


//Controller for User Registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else {
			return true;
		};
	});
};

//Controllers for user authentication(login)

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result==null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};


//Controller for retrieving user details

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};



// //Test Controller Create Order

// module.exports.createOrder = async (data) => {
//   try {
//     let isAdmin = await User.findById(data.userId).then(user => {
//       return user.isAdmin;
//     });

//     if (isAdmin) {
//       return "Admin access cannot place order";
//     } else {
//       let isUserUpdated = await User.findById(data.userId);
//       let isProductUpdated = await Product.findById(data.productId);

//       let quantity = isUserUpdated.orderedProduct.length + 1;
//       let totalAmount = isProductUpdated.price * quantity;

//       isUserUpdated.orderedProduct.push({

//       	products: [
//       	{
//         productId: data.productId,
//         productName: isProductUpdated.name,
//         quantity: quantity
//     	}
//     	],
//         totalAmount: totalAmount
//       });

//       await isUserUpdated.save();

//       isProductUpdated.userOrders.push({ userId: data.userId});
//       await isProductUpdated.save();

//       return true;
//     }
//   } catch (error) {
//     // Handle any errors that occur during the process
//     console.error(error);
//     throw error;
//   }
// };


// Test controller create order

module.exports.createOrder = async (data) => {
  try {
    let isAdmin = await User.findById(data.userId).then(user => {
      return user.isAdmin;
    });

    if (isAdmin) {
      return "Admin access cannot place an order.";
    } else {
      let isUserUpdated = await User.findById(data.userId);
      let isProductUpdated = await Product.findById(data.productId);

      if (!isProductUpdated) {
        throw new Error("Product not found.");
      }

      let quantity = isUserUpdated.orderedProduct.length + 1;
      let totalAmount = isProductUpdated.price * quantity;

      isUserUpdated.orderedProduct.push({
        products: [
          {
            productId: data.productId,
            productName: isProductUpdated.name,
            quantity: quantity
          }
        ],
        totalAmount: totalAmount
      });

      await isUserUpdated.save();

      isProductUpdated.userOrders.push({
   		userId: data.userId
   		});
      await isProductUpdated.save();

      return true;
    }
  } catch (error) {
    // Handle any errors that occur during the process
    console.error(error);
    throw error;
  }
};
