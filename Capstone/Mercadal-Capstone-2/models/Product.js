const mongoose = require ("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
			},
	description: {
		type: String,
		required: [true, "Product description is required"]
			},
	price: {
		type: Number,
		required: [true, "Price is required"]
			},
	isActive: {
		type: Boolean,
		default: true
			},
	createdOn: {
		type:Date,
		default: new Date()
			},
	userOrders: [{
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			required: [true, "UserID is required"]
			},
		orderId: {
			type: String,
			}
		}]
});


module.exports = mongoose.model("Product", productSchema);
