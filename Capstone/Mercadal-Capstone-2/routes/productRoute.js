// const express = require("express");

// const router = express.Router();

// const productController = require("../controllers/productController")

// const auth = require("../auth");

// // Route for creating product(admin only)
// router.post("/create", auth.verify, (req, res) => {

// 	const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
// });


// // Route for retrieving all the products(Admin only)
// router.get("/all", auth.verify, (req, res) => {
// 	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
// });

// // Route for retrieving all active products
// router.get("/active", (req, res) => {
// 	productController.getAllActive().then(resultFromController => res.send(resultFromController));
// });


// // Route for retrieving single product
// router.get("/:productId", (req, res) => {
// 	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
// });


// // Route for updating a product (Admin Only)
// router.put("/:productId", auth.verify, (req, res) => {
// 	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
// });


// // Route for archiving a product (Admin Only)
// router.patch("/:productId/archive", auth.verify, (req, res) => {
// 	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
// });



// module.exports = router;


//test new code

const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product (admin only)
router.post("/create", auth.verify, auth.isAdmin, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products (Admin only)
router.get("/all", auth.verify, auth.isAdmin, (req, res) => {
  productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products (No authentication required)
router.get("/active", (req, res) => {
  productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving single product (No authentication required)
router.get("/:productId", (req, res) => {
  productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product (Admin Only)
router.put("/:productId", auth.verify, auth.isAdmin, (req, res) => {
  productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archiving a product (Admin Only)
router.patch("/:productId/archive", auth.verify, auth.isAdmin, (req, res) => {
  productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for reactivating a product (Admin Only)
router.patch("/:productId/reactivate", auth.verify, auth.isAdmin, (req, res) => {
  productController.reactivateProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for deleting a product (Admin Only)
router.delete("/:productId", auth.verify, auth.isAdmin, (req, res) => {
  productController.deleteProduct(req.params.productId)
    .then(resultFromController => {
      if (resultFromController) {
        res.status(200).json({ message: "Product deleted successfully." });
      } else {
        res.status(404).json({ error: "Product not found." });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Error deleting product.", details: error });
    });
});
module.exports = router;
