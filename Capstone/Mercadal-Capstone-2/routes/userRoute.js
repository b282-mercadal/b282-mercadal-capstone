const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController")

const auth = require("../auth");

// Routes for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration

router.post("/register", (req, res)=> {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Rute for user authentication (login)
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route to create order
router.post("/createOrder", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    productId:req.body.productId
  };

  userController.createOrder(data)
    .then(resultFromController => {
      res.send(resultFromController);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send("An error occurred while creating the order.");
    });
});




module.exports = router;