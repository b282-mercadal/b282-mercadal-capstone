import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  const navigate = useNavigate(); // Get the navigate function from the hook

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);

          if (data.isAdmin) {
            setIsAdmin(true);
          }

          retrieveUserDetails(data.access);

          Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome to My Store',
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed',
            icon: 'error',
            text: 'Please, check your login details and try again.',
          });
        }
      });

    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      })
      .catch((error) => {
        console.error('Error fetching user details:', error);
      });
  };

  useEffect(() => {
    if (isAdmin && user.id) {
      Swal.fire({
        title: 'Hello Admin',
        icon: 'info',
        text: 'You are logged in as an admin.',
      });
    } else if (user.id) {
      navigate('/products'); // Redirect to products page if user is logged in (non-admin)
    }
  }, [isAdmin, navigate, user.id]);

  return (
    <>
      <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        <br />

        {isActive ? (
          <Button variant="success" type="submit" id="loginBtn">
            Login
          </Button>
        ) : (
          <Button variant="secondary" type="submit" id="loginBtn" disabled>
            Login
          </Button>
        )}
      </Form>
    </>
  );
}
