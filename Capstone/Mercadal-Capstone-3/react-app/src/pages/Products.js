// import ProductCard from '../components/ProductCard';
// import {useState, useEffect} from 'react';

// export default function Products() {
	
// 	const [products, setProducts] = useState([]);

// 	useEffect(() => {
// 		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
// 		.then(res => res.json())
// 		.then(data => {
// 			setProducts(data.map(product => {
// 				return (
// 					<ProductCard key={product.id} product = {product} />
// 				)
// 			}))
// 		})
// 	}, []);

// 	return (
// 		<>
// 		{products}
// 		</>
// 	)
// };


import ProductCard from '../components/ProductCard';
import { useState, useEffect } from 'react';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  }, []);

  return (
    <div>
      <h2>Products</h2>
      <div className="product-list">
        {products.map(product => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </div>
  );
}
