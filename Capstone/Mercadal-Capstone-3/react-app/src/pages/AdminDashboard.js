import React, { useState, useEffect } from 'react';
import { Table, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const AdminDashboard = () => {
  // States for managing products
  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0, isActive: true });
  const [editProduct, setEditProduct] = useState(null);

  // Function to fetch the list of products
  const fetchProducts = () => {
    const token = localStorage.getItem('token');
    
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          console.log(data); // Check if able to pull up necessary data
          setProducts(data);
        })
        .catch(error => console.error('Error fetching products:', error));
    } else {
      console.error('User is not authenticated. No token found in localStorage.');
    }
  };

  // Function to handle form submission for adding or updating a product
  const handleSubmit = (e) => {
    e.preventDefault();

    const token = localStorage.getItem('token');

    if (editProduct) {
      // Update existing product
      fetch(`${process.env.REACT_APP_API_URL}/products/${editProduct._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`, // Include the token in the request headers
        },
        body: JSON.stringify(newProduct),
      })
        .then(response => {
          // Clear the form and reload the list of products
          setNewProduct({ name: '', description: '', price: 0, isActive: true });
          setEditProduct(null);
          fetchProducts(); // Fetch the updated list of products

          // Show sweet alert for successful update
          Swal.fire({
            icon: 'success',
            title: 'Product Updated!',
            text: 'The product has been successfully updated.',
          });
        })
        .catch(error => console.error('Error updating product:', error));
    } else {
      // Add new product
      fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`, // Include the token in the request headers
        },
        body: JSON.stringify(newProduct),
      })
        .then(response => {
          // Clear the form and reload the list of products
          setNewProduct({ name: '', description: '', price: 0, isActive: true });
          fetchProducts(); // Fetch the updated list of products

          // Show sweet alert for successful addition
          Swal.fire({
            icon: 'success',
            title: 'Product Added!',
            text: 'The product has been successfully added.',
          });
        })
        .catch(error => console.error('Error creating product:', error));
    }
  };

  // Function to handle product deactivation/reactivation
  const handleToggleProduct = (productId, isActive) => {
    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${isActive ? 'archive' : 'reactivate'}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${token}`, // Include the token in the request headers
      },
    })
      .then(response => {
        fetchProducts(); // Fetch the updated list of products
      })
      .catch(error => console.error('Error toggling product status:', error));
  };

  // Function to handle product edit
  const handleEditProduct = (product) => {
    // Set the editProduct state to the selected product
    setEditProduct(product);

    // Set the newProduct state with the selected product's details
    setNewProduct({
      name: product.name,
      description: product.description,
      price: product.price,
      isActive: product.isActive,
    });
  };

  // Function to handle product delete
  const handleDeleteProduct = (productId) => {
    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`, // Include the token in the request headers
      },
    })
      .then(response => {
        fetchProducts(); // Fetch the updated list of products
      })
      .catch(error => console.error('Error deleting product:', error));
  };

  // Function to handle clearing the form fields when "Add New Product" button is clicked
  const handleClearForm = () => {
    setEditProduct(null);
    setNewProduct({ name: '', description: '', price: 0, isActive: true });
  };

  // Fetch the list of products when the component mounts
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div>
      <h2>Admin Dashboard</h2>

      {/* Button to add new product */}
      <Button variant="primary" onClick={handleClearForm}>Add New Product</Button>
      <br/>

      {/* Table to display list of products */}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(products) && products.map((product) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>${product.price}</td>
              <td>{product.isActive ? 'Active' : 'Inactive'}</td>
              <td>
                <Button variant="primary" onClick={() => handleEditProduct(product)}>
                  Edit
                </Button>{' '}
                <Button
                  variant={product.isActive ? 'danger' : 'success'}
                  onClick={() => handleToggleProduct(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Reactivate'}
                </Button>{' '}
                <Button variant="danger" onClick={() => handleDeleteProduct(product._id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* Form to add new product or update existing product */}
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product name"
            value={newProduct.name}
            onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product description"
            value={newProduct.description}
            onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formPrice">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter product price"
            value={newProduct.price}
            onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formStatus">
          <Form.Check
            type="checkbox"
            label="Active"
            checked={newProduct.isActive}
            onChange={(e) => setNewProduct({ ...newProduct, isActive: e.target.checked })}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          {editProduct ? 'Update Product' : 'Add Product'}
        </Button>
      </Form>
    </div>
  );
};

export default AdminDashboard;
