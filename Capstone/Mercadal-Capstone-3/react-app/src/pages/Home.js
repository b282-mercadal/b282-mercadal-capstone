import Banner from '../components/Banner';
//import Highlights from '../components/Highlights';


// export default function Home() {
// 	return (

// 		<>
// 		<Banner 
// 			title="Zuitt Coding Bootcamp"
// 			subtitle="Opportunities for everyone, everywhere."
// 			buttonText="Enroll now!"
// 			buttonVariant="primary" />
// 		<Highlights />
// 		</>

// 	)
// }

export default function Home() {

	const data = {
		title: "Welcome to my Laptop Store",
		content: "Buy from the comfort of your home.",
		destination: "/products",
		label: "Check what's in store!"
	}


	return (
		<>
		<Banner data={data} />
    	{/*<Highlights />*/}
    	
		</>
	)
}


