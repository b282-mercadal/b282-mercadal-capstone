// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

// import {useState, useContext} from 'react'

// import {Link, NavLink} from 'react-router-dom';

// import UserContext from '../UserContext';

// function AppNavbar() {


// const {user} = useContext(UserContext);

//   return (
//     <Navbar expand="lg" className="bg-body-tertiary">
//         <Navbar.Brand as={Link} to="/">Laptop Store</Navbar.Brand>
//         <Navbar.Toggle aria-controls="basic-navbar-nav" />
//         <Navbar.Collapse id="basic-navbar-nav">
//           <Nav className="me-auto">
//             <Nav.Link as={NavLink} to="/">Home</Nav.Link>
//             <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

//             { (user.id) ?
//               <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
//               :
//               <>
//               <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
//               <Nav.Link as={NavLink} to="/login">Login</Nav.Link>            
//               </>
//             }
                        
//           </Nav>
//         </Navbar.Collapse>
//     </Navbar>
//   );
// }

// export default AppNavbar;

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { useContext } from 'react';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Navbar.Brand as={Link} to="/">
        Laptop Store
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/products">
            Products
          </Nav.Link>

          {user.isAdmin && user.id && (
            <Nav.Link as={NavLink} to="/admin">
              Admin Dashboard
            </Nav.Link>
          )}

          {user.id ? (
            <Nav.Link as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default AppNavbar;
