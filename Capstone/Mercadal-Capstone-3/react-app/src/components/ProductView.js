// import {useState, useContext, useEffect} from 'react';

// import { Container, Card, Button, Row, Col } from 'react-bootstrap';

// import {useParams, Link, useNavigate} from 'react-router-dom';

// import UserContext from '../UserContext';

// import Swal from 'sweetalert2';

// export default function ProductView() {

// 	const {user} = useContext(UserContext);

// 	const navigate = useNavigate();

// 	// useParams() is a hook that will allows us to retrieve productId passed via URL params
// 	const {productId} = useParams();

// 	const [name, setName] = useState("");
// 	const [description, setDescription] = useState("");
// 	const [price, setPrice] = useState(0);

// 	const purchase = (productId) => {
// 			fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
// 				method: "POST",
// 				headers: {
// 					'Content-Type': 'application/json',
// 					Authorization: `Bearer ${localStorage.getItem('token')}`
// 				},
// 				body: JSON.stringify({
// 					productId:productId
// 				})
// 			})
// 			.then(res => res.json())
// 			.then(data => {
// 				console.log(data)

// 				if(data === true) {
// 					Swal.fire({
// 						title: "Successfully checked out!",
// 						icon: "success",
// 						text: "Item successfully checked out."
// 					})

// 					navigate("/products");

// 				} else {
// 					Swal.fire({
// 						title: "Something went wrong",
// 						icon: "error",
// 						text: "Please try again."
// 					})
// 				}

// 			})
// 		};

// 	useEffect(() => {
// 		console.log(productId);

// 		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data);
// 			setName(data.name);
// 			setDescription(data.description);
// 			setPrice(data.price);
// 		})
// 	}, [productId]);

// 	return (

// 		<Container>
// 		      <Row>
// 		        <Col lg={{ span: 6, offset: 3 }}>
// 		          <Card>
// 		            <Card.Body className="text-center">
// 		              <Card.Title>{name}</Card.Title>
// 		              <Card.Subtitle>Description:</Card.Subtitle>
// 		              <Card.Text>{description}</Card.Text>
// 		              <Card.Subtitle>Price:</Card.Subtitle>
// 		              <Card.Text>PhP {price}</Card.Text>

// 		              {/* Conditionally render the Checkout button */}
// 		              {user.id !== null && !user.isAdmin ? (
// 		                <Button variant="primary" onClick={() => purchase(productId)}>
// 		                  Checkout
// 		                </Button>
// 		              ) : (
// 		                <Button className="btn btn-danger" as={Link} to="/login">
// 		                  {user.isAdmin ? "Admin cannot checkout" : "Log in to Checkout"}
// 		                </Button>
// 		              )}
// 		            </Card.Body>
// 		          </Card>
// 		        </Col>
// 		      </Row>
// 		    </Container>
// 		  );
// 		}

import React, { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const handleQuantityChange = (e) => {
    const value = e.target.value;
    if (!isNaN(value) && parseInt(value) >= 1) {
      setQuantity(parseInt(value));
    }
  };

  const calculateTotalPrice = () => {
    return price * quantity;
  };

  const purchase = (productId, quantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Successfully checked out!',
            icon: 'success',
            text: 'Item successfully checked out.',
          });

          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  return (
    <Container>
      <Row>
        <Col lg={{ span: 8, offset: 2 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              {/* Conditionally render the Quantity input and Total price */}
              {!user.isAdmin && (
                <>
                  <Form.Group controlId="quantity">
                    <Form.Label>Quantity:</Form.Label>
                    <Form.Control
                      type="number"
                      value={quantity}
                      onChange={handleQuantityChange}
                      min="1"
                      step="1"
                    />
                  </Form.Group>

                  <Card.Subtitle>Total Price:</Card.Subtitle>
                  <Card.Text>PhP {calculateTotalPrice()}</Card.Text>
                </>
              )}

              {/* Conditionally render the Checkout button */}
              {user.id !== null && !user.isAdmin ? (
                <Button variant="primary" onClick={() => purchase(productId, quantity)}>
                  Checkout
                </Button>
              ) : (
                <Button className="btn btn-danger" as={Link} to="/login">
                  {user.isAdmin ? 'Admin cannot checkout' : 'Log in to Checkout'}
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
