//App.js
import './App.css';
import { useState, useEffect } from 'react';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';

import AdminDashboard from './pages/AdminDashboard';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import UserContext from './UserContext';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import {Navigate} from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

const checkLoginStatus = () => {
  const token = localStorage.getItem('token');
  console.log('Token:', token);

  if (token) {
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to fetch user details.');
        }
        return res.json();
      })
      .then((data) => {
        // user is logged in
        if (typeof data._id !== 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          // user is logged out
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      })
      .catch((error) => {
        console.error('Error fetching user details:', error);
        // Handle error here (e.g., show an error message to the user)
      });
  } else {
    // If token doesn't exist, user is not logged in
    setUser({
      id: null,
      isAdmin: null,
    });
  }
};

useEffect(() => {
  checkLoginStatus();
}, []);



  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/*" element={<Error />} />
              <Route path="/products" element={<Products />} />
              <Route path="/admin" element={<AdminDashboard />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;



